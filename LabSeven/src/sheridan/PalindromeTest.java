package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid input", Palindrome.isPalindrome("Anna"));
	}

	@Test
	public void testIsNotPalindrome() {
		assertFalse("Is not a palindrome.", Palindrome.isPalindrome("Annda"));
	}

	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid input", Palindrome.isPalindrome("a"));
	}

	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("Invalid input", Palindrome.isPalindrome("Anna Chamon"));
	}

}
